//
//  TextFieldRadius.swift
//  FarhaProject
//
//  Created by Mahmoud Khaled on 10/14/18.
//  Copyright © 2018 Mahmoud Khaled. All rights reserved.
//

import UIKit

@IBDesignable
class TextFieldRadius: UITextField {

    @IBInspectable var cornerRadius: CGFloat = 3.0{
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable var borderColer:CGColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1){
        didSet{
            layer.borderColor = borderColer
        }
    }
    @IBInspectable var borderWidth: CGFloat = 2.7{
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        layer.borderColor = borderColer
        layer.borderWidth = borderWidth
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
