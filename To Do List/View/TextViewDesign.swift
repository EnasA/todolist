//
//  TextViewDesign.swift
//  Farah
//
//  Created by Mahmoud Khaled on 11/28/18.
//  Copyright © 2018 Mahmoud Khaled. All rights reserved.
//

import UIKit

@IBDesignable
class TextViewDesign: UITextView {

    @IBInspectable var cornerRadius : CGFloat = 2.7 {
        didSet{
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor : UIColor? {
        didSet{
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet {
            layer.borderWidth = borderWidth
        }
    }

    
   
    
    
}
