//
//  ToDoItemTableViewCell.swift
//  To Do List
//
//  Created by enas on 12/28/18.
//  Copyright © 2018 enas. All rights reserved.
//

import UIKit

class ToDoItemTableViewCell: UITableViewCell {

    @IBOutlet weak var TaskNameLebel: UILabel!
    var task: ToDoListItem?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    
    func updateCell(task : ToDoListItem ){
        self.task = task
        TaskNameLebel.text = task.name
    }
    

}
