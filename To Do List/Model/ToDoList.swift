//
//  ToDoList.swift
//  To Do List
//
//  Created by enas on 12/28/18.
//  Copyright © 2018 enas. All rights reserved.
//

import Foundation
import RealmSwift
import Realm


class ToDoList {
    static func allTasks() -> Results<ToDoListItem> {
        let realm = try! Realm()
        return realm.objects(ToDoListItem.self)
    }
}
