//
//  ToDoGeneralMethods.swift
//  To Do List
//
//  Created by enas on 12/28/18.
//  Copyright © 2018 enas. All rights reserved.
//

import Foundation
import UIKit
class ToDoGeneralMethods {
    
    static let instance = ToDoGeneralMethods()


    
    
    //create VavigationBar Push ViewController
    public class  func pushViewController(navController: UINavigationController , vControllerIdentifier : String ) -> UIViewController{
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: vControllerIdentifier)
        navController.pushViewController(vc, animated: false)
        
        return vc
    }
    
    
    
    
    
    
}
