//
//  AddItemViewController.swift
//  To Do List
//
//  Created by enas on 12/28/18.
//  Copyright © 2018 enas. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

   //Outlets
    @IBOutlet weak var taskNameTF: UITextField!
    @IBOutlet weak var taskDescrTV: UITextView!
    
    
    //Variables:-
    var task: ToDoListItem?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    taskDescrTV.placeholder = "Enter Task Descriptions"
       
    }


    @IBAction func CancelBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }
    
    
    
    @IBAction func SaveBtnPressed(_ sender: Any) {
        
        
        // Get Values From TextFields
        let taskName = self.taskNameTF!.text
        let taskDescr = self.taskDescrTV!.text
       
        
        //validate inpuet Values
        if ( taskName?.isEmpty )! {
            self.taskNameTF.layer.borderColor = UIColor.red.cgColor
            return
        } else {
            self.taskNameTF.layer.borderColor = UIColor.black.cgColor
        }
        if ( taskDescr?.isEmpty )! {
            self.taskDescrTV.layer.borderColor = UIColor.red.cgColor
            return
        } else {
            self.taskDescrTV.layer.borderColor = UIColor.black.cgColor
        }
        
        task = ToDoListItem()
        task?.name = taskNameTF.text ?? ""
        task?.Desc = taskDescrTV.text ?? ""
        task?.save()
        let navVC = storyboard?.instantiateViewController(withIdentifier: "ListItem")
        self.present(navVC!, animated: false, completion: nil)



        
        
    }
    
    
    

}
