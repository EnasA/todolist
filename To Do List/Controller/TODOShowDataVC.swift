//
//  TODOShowDataVC.swift
//  To Do List
//
//  Created by enas on 12/29/18.
//  Copyright © 2018 enas. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import UserNotifications

class TODOShowDataVC: UIViewController  {

  
    @IBOutlet weak var taskTitleLabel: UILabel!
    @IBOutlet weak var TaskDescrLebel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    //Variables:-
    
    var locationManager = CLLocationManager()
    var TaskTitle:String?
    var TaskDesc:String?
    var latitude :Double?
    var longitude:Double?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        self.checkForCurrentUserLocation()
        

        if TaskTitle != ""  , TaskDesc != ""  {
            taskTitleLabel.text = TaskTitle
            TaskDescrLebel.text = TaskDesc
        }
        
        // Handell Notiycations:--
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in }

        
        
        
        
    }

    
    
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    
   
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

//
   
    // LongTapGesture To Make Mointering
    @IBAction func AddRegion(_ sender: Any) {
        print("Pressed")
        guard let longPress = sender as? UILongPressGestureRecognizer else { return }
        let touchLocation = longPress.location(in: mapView)
        let locationCoordinate = mapView.convert(touchLocation,toCoordinateFrom: mapView)
        print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
        let geofenceRegion  = CLCircularRegion(center: locationCoordinate, radius: 200, identifier: "geofence")
        mapView.removeOverlays(mapView.overlays)
        locationManager.startMonitoring(for: geofenceRegion )
        let circle = MKCircle(center: locationCoordinate, radius: geofenceRegion.radius)
        mapView.add(circle)
        
        
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
   
    
    //MARK:-  Handel Local Notifications : --
    func showNotification(title: String, message: String) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.badge = 1
        content.sound = .default()
        let request = UNNotificationRequest(identifier: "notif", content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
}
    
   

extension TODOShowDataVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let circleOverlay = overlay as? MKCircle else { return MKOverlayRenderer() }
        let circleRenderer = MKCircleRenderer(circle: circleOverlay)
        circleRenderer.strokeColor = .red
        circleRenderer.fillColor = .red
        circleRenderer.alpha = 0.5
        return circleRenderer
    }
}

extension TODOShowDataVC : CLLocationManagerDelegate{
    
    ////// check for current user location
    func checkForCurrentUserLocation() {
        //Check for Location Services
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            
            var currentLocation = self.locationManager.location
            currentLocation = self.locationManager.location
        }
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        //Zoom to user location
        let noLocation = CLLocationCoordinate2D()
        let viewRegion = MKCoordinateRegionMakeWithDistance(noLocation, 200, 200)
        mapView.setRegion(viewRegion, animated: true)
        
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else { return }
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    
    
    
    //Receive monitoring events in SaveRegion
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        let title = "You Done the Task"
        let message = "Congratulations :-!"
        showAlert(title: title, message: message)
        showNotification(title: title, message: message)
    }
    //Receive monitoring events in OUTRegion

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        let title = "Task Not Done"
        let message = "Try To Do it"
        showAlert(title: title, message: message)
        showNotification(title: title, message: message)
    }
    
    
    
    
    
    
}
