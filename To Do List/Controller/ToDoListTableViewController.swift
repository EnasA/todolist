//
//  ToDoListTableViewController.swift
//  To Do List
//
//  Created by enas on 12/28/18.
//  Copyright © 2018 enas. All rights reserved.
//

import UIKit

class ToDoListTableViewController: UIViewController {

    //Outlets:-
    
    @IBOutlet weak var ToDoListTableView: UITableView!
    
    //Variables:-
    
     var tasks = [ToDoListItem]()
    let realmManager = ToDoListItem()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for task in ToDoList.allTasks() {
            tasks.append(task)
        }
    }

    
    
    @IBAction func AddBtnPressed(_ sender: UIBarButtonItem) {
        
       let navVC = storyboard?.instantiateViewController(withIdentifier: "AddItem")
        self.present(navVC!, animated: false, completion: nil)
    }
        
   

    



}


extension ToDoListTableViewController :UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = ToDoListTableView.dequeueReusableCell(withIdentifier: "ToDoItemTableViewCell", for: indexPath) as? ToDoItemTableViewCell  else {
            fatalError("The dequeued cell is not an instance of ToDoItemTableViewCell.")
        }
        
        let task = tasks[indexPath.row]
        cell.updateCell(task: task)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let TaskName = tasks[indexPath.row].name
        print(TaskName)
        let TaskDesc = tasks[indexPath.row].Desc
        let navVC = storyboard?.instantiateViewController(withIdentifier: "TODOShowDataVC") as! TODOShowDataVC
        navVC.TaskTitle = TaskName
        navVC.TaskDesc = TaskDesc
        self.present(navVC, animated: false, completion: nil)
        
        
    }
    
   
    
    
    
    
//    //MARK:- Swipe Ations 
//    
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let EditService = EditServiceAction(at:indexPath)
//        let deleteServices  = deletAction(at:indexPath)
//        let config =  UISwipeActionsConfiguration(actions: [EditService , deleteServices])
//        config.performsFirstActionWithFullSwipe = false
//        return config
//    }
//    
//    // Edit Actions :--
//    func EditServiceAction(at indexPath: IndexPath) -> UIContextualAction {
//        
//        let action = UIContextualAction(style: .normal, title: "Editing") { (action, view, completion) in
//        
//            completion(true)
//        }
//       
//        
//        action.backgroundColor = .gray
//        return action
//    }
//    
//   
//    // Delete Actions :--
//    func deletAction(at indexPath: IndexPath) -> UIContextualAction {
//        
//        
//        let action = UIContextualAction(style: .normal, title: "Delete") { (action, view, completion) in
//            print("DeleteSupply")
//            let task : ToDoListItem = self.tasks[indexPath.row]
//            // delete an Object
//            print(task)
//            self.realmManager.deleteObject(objs: task)
//            self.tasks.remove(at: indexPath.row)
//            self.ToDoListTableView.deleteRows(at: [indexPath],  with: UITableViewRowAnimation.automatic)
//            self.ToDoListTableView.reloadData()
//            
//            completion(true)
//        }
//        
//        action.backgroundColor = .red
//        return action
//        
//        
//    }
//    
//    
//    
//    
//    
//    
//    
    
    
}




